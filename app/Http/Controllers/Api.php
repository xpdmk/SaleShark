<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\VerkkokauppaScraper;
use App\VerkkokauppaImporter;
use App\Model\Sale;
use App\Http\Resources\Sale as SaleResource;
use App\Model\Location;
use App\Http\Resources\Location as LocationResource;



class Api extends Controller
{

    public function getSales(Request $request){

        $parameters = $request->all();

        // TODO: Add subquery syntax (for sales table, for example store.location_id)
	    // for parameters or change implementation
	    // to use q parameter to have better structure for querying sales

	    // Check parameters if needed

	    return new SaleResource(
		    Sale::where($parameters)
			    ->with(["store.location","store.seller", "product"])
			    ->get()
	    );

//        //Whitelisted keys
//        $keys = [
//            'name',
//            'sale_id',
//            'price',
//            'original_price',
//            'origin',
//            'origin_id',
//            'product_id',
//            'product_name',
//            'seller_id',
//            'seller_name',
//            'origin_id',
//            'origin_name',
//            'location_id',
//            'location_name',
//            'store_id',
//            'store_name'
//        ];
//
//        $sales = DB::table('sales')
//            ->join('stores', 'sales.store_id', '=', 'stores.store_id')
//            ->join('products', 'sales.product_id', '=', 'products.product_id')
//            ->join('origins', 'sales.origin_id', '=', 'origins.origin_id')
//            ->join('locations', 'stores.location_id', '=', 'locations.location_id')
//            ->join('sellers', 'stores.seller_id', '=', 'sellers.seller_id')
//            ->select('*');
//
//
//        $failedkeys = [];
//        foreach($parameters as $key => $value){
//            if(in_array($key, $keys)){
//                $sales = $sales->where($key, $value);
//            } else {
//                array_push($failedkeys, $key);
//            }
//        }
//
//
//        if(count($failedkeys)>0){
//            $failmessage = "Invalid keys: " . json_encode($failedkeys);
//            return $failmessage;
//        }
//
//        return json_encode($sales->get());
    }

    public function postSales(Request $request){
        $VS = new VerkkokauppaScraper();
        $origPrice = $request['original_price'];
        $sale = $request['price'];
        $name = $request['product_name'];
        $amount = $request['amount'];
        $category = $request['category'];
        $brand = $request['brand'];
        $url = $request['link'];
        $origPrice = $VS->stringToNumber($origPrice);
        $sale = $VS->stringToNumber($sale);

        $saleDetails = array(array(
            'product_name' => $name,
            'original_price' => $origPrice,
            'price' => $sale,
            'amount'=> $amount,
            'category' => $category,
            'brand' => $brand,
            'link' => $url
        ));

        VerkkokauppaImporter::importProducts($saleDetails,2);

    }
    public function scrape(){
        include $_SERVER['DOCUMENT_ROOT'].'\..\StartScraping.php';
    }
}
