<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
	protected $fillable = ['store_name'];
	protected $primaryKey = "store_id";
	protected $table = "stores";
	public $timestamps = false;

	public function location() {
		return $this->hasOne("App\Model\Location", "location_id", "location_id");
	}

	public function seller() {
		return $this->hasOne("App\Model\Seller", "seller_id", "seller_id");
	}

	public function sales() {
		return $this->belongsTo("App\Model\Sale", "sale_id", "sale_id");
	}
}
