<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
	protected $fillable = ["seller_name"];
	protected $primaryKey = "seller_id";
	protected $table = "sellers";
	public $timestamps = false;

	public function stores() {
		return $this->belongsTo("App\Model\Store", "store_id", "store_id");
	}
}
