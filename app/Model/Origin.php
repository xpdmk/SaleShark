<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Origin extends Model
{
	protected $fillable = ['origin_id', 'origin_name'];
	protected $primaryKey = "origin_id";
	protected $table = "origins";
	public $timestamps = false;

	public function sales() {
		return $this->belongsTo("App\Model\Sale", "sale_id", "sale_id");
	}
}
