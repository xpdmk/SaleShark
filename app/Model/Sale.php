<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
	protected $fillable = ["price","original_price","link","amount"];
	protected $primaryKey = "sale_id";
	protected $table = "sales";
	public $timestamps = false;

	public function store() {
		return $this->hasOne("App\Model\Store", "store_id", "store_id");
	}

	public function origin() {
		return $this->hasOne("App\Model\Origin", "origin_id", "origin_id");
	}

	public function product() {
		return $this->hasOne("App\Model\Product", "product_id", "product_id");
	}

	public function percent() {
		return round(
			($this->original_price/$this->price)-100
		);
	}
}
