<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	protected $fillable = ["product_name","brand", "category"];
	protected $primaryKey = "product_id";
	protected $table = "products";
	public $timestamps = false;

	public function sales() {
		return $this->belongsTo("App\Model\Sale", "sale_id", "sale_id");
	}
}
